using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInformation : MonoBehaviour
{
    /// Collision Section
    public GameObject screenTop;
    public GameObject screenBottom;
    public GameObject screenLeft;
    public GameObject screenRight;

    public GameObject screenShipTop;
    public GameObject screenShipBottom;
    public GameObject screenShipLeft;
    public GameObject screenShipRight;


    public static Vector3 MinimumScreenEdge = Vector3.zero;
    public static Vector3 MaximumScreenEdge = Vector3.zero;
    public static Vector3 MinimumScreenShipEdge = Vector3.zero;
    public static Vector3 MaximumScreenShipEdge = Vector3.zero;
    /// Collision Section

    void Awake()
    {
        MaximumScreenEdge.z = screenTop.transform.position.z;
        MaximumScreenEdge.x = screenRight.transform.position.x;
        MinimumScreenEdge.z = screenBottom.transform.position.z;
        MinimumScreenEdge.x = screenLeft.transform.position.x;

        MaximumScreenShipEdge.z = screenShipTop.transform.position.z;
        MaximumScreenShipEdge.x = screenShipRight.transform.position.x;
        MinimumScreenShipEdge.z = screenShipBottom.transform.position.z;
        MinimumScreenShipEdge.x = screenShipLeft.transform.position.x;
    }

}
