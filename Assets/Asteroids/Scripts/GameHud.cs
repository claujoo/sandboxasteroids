using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHud : MonoBehaviour
{
    public Text scoreText;
    public Text levelText;
    public GameObject gameOverText;
    public Text nextLevelText;
    public GameObject [] livesIcons;
    public GameObject menuCanvas;

    void Awake()
    {
        gameOverText.SetActive(false);
        nextLevelText.gameObject.SetActive(false);
        foreach (GameObject obj in livesIcons)
            obj.SetActive(false);
    }

    void Update()
    {
        if (GamePlay.Myself.state == GamePlay.GameState.gamePlaying)
        {
            if (scoreText.text != ("SCORE: " + GamePlay.Myself.score.ToString("D6")))
                scoreText.text = "SCORE: " + GamePlay.Myself.score.ToString("D6");
            if (levelText.text != ("LEVEL " + GamePlay.Myself.level.ToString()))
                levelText.text = "LEVEL " + GamePlay.Myself.level.ToString();
            if (GamePlay.Myself.lastLevel != GamePlay.Myself.level)
            {
                GamePlay.Myself.lastLevel = GamePlay.Myself.level;
                StartCoroutine(NextLevelDisplay());
            }
        }

        if (GamePlay.Myself.state != GamePlay.GameState.mainMenu)   // Lives may change anytime but during the Main Menu
        {
            if (GamePlay.Myself.lastLives != GamePlay.Myself.lives)
            {
                GamePlay.Myself.lastLives = GamePlay.Myself.lives;
                for (int i= 0; i < livesIcons.Length;i++)
                {
                    if ((GamePlay.Myself.lives - 1) > i)
                        livesIcons[i].SetActive(true);
                    else
                        livesIcons[i].SetActive(false);
                }
            }
        }

        if (GamePlay.Myself.state == GamePlay.GameState.gameOver)
        {
            StartCoroutine(GameOverSequence());
            GamePlay.Myself.state = GamePlay.GameState.mainMenu;
        }
    }

    // TIMED ACTIONS
    // TIMED ACTIONS
    IEnumerator GameOverSequence()
    {
        MusicManager.Myself.GameOver();

        gameOverText.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        gameOverText.SetActive(false);
        menuCanvas.SetActive(true);
    }

    IEnumerator NextLevelDisplay()
    {
        MusicManager.Myself.NextLevel();

        nextLevelText.text = "Ready for Level " + GamePlay.Myself.level;
        nextLevelText.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        nextLevelText.gameObject.SetActive(false);
    }
}
