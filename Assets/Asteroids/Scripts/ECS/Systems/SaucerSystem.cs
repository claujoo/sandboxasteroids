using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class SaucerSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if ((GamePlay.Myself.state != GamePlay.GameState.gamePlaying) && (GamePlay.Myself.state != GamePlay.GameState.shieldShip)) return;

        Entities.WithAll<Saucer>().ForEach((ref Translation translation, ref Rotation rotation, ref Saucer saucer) =>
        {
            if (GamePlay.Myself.state != GamePlay.GameState.shieldShip)
            {
                // collide with player
                if (Player.Myself.CollisionEnemy(translation.Value, saucer.collisionRadius))
                    return;
            }
            saucer.shootTimer -= Time.DeltaTime;
            if (saucer.shootTimer < 0.0f)
            {
                SpawnSaucerAndBonus.Myself.SpawnSaucerBullet(translation.Value);
                saucer.shootTimer = GamePlay.SaucerShootInterval;
            }
            // Move entity
            translation.Value.x += saucer.dx * Time.DeltaTime * 60.0f;
            translation.Value.y += saucer.dy * Time.DeltaTime * 60.0f;
            translation.Value.z += saucer.dz * Time.DeltaTime * 60.0f;
            if (translation.Value.x > GameInformation.MaximumScreenEdge.x)
                translation.Value.x -= (GameInformation.MaximumScreenEdge.x - GameInformation.MinimumScreenEdge.x);
            if (translation.Value.x < GameInformation.MinimumScreenEdge.x)
                translation.Value.x += (GameInformation.MaximumScreenEdge.x - GameInformation.MinimumScreenEdge.x);
            if (translation.Value.z > GameInformation.MaximumScreenEdge.z)
                translation.Value.z -= (GameInformation.MaximumScreenEdge.z - GameInformation.MinimumScreenEdge.z);
            if (translation.Value.z < GameInformation.MinimumScreenEdge.z)
                translation.Value.z += (GameInformation.MaximumScreenEdge.z - GameInformation.MinimumScreenEdge.z);
        });
    }
}

