using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class StarSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        Entities.WithAll<Star>().ForEach((ref Translation translation, ref Rotation rotation, ref Star star) =>
        {
            translation.Value.y += star.speed * Time.DeltaTime;
            if (translation.Value.y > 50.0f)
                translation.Value.y = -100.0f;
        });
    }
}