using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class RockSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if ((GamePlay.Myself.state != GamePlay.GameState.gamePlaying) && (GamePlay.Myself.state != GamePlay.GameState.shieldShip)) return;

        Entities.WithAll<Rock>().ForEach((ref Translation translation, ref Rotation rotation, ref Rock rock) =>
        {
            // collide with player
            if (GamePlay.Myself.state != GamePlay.GameState.shieldShip)
                Player.Myself.CollisionEnemy(translation.Value, rock.collisionRadius);

            // Rotate entity
            quaternion q1 = quaternion.RotateY(rock.drotatex * Time.DeltaTime);
            quaternion q2 = quaternion.RotateX(rock.drotatey * Time.DeltaTime);
            rotation.Value = math.mul(math.mul(rotation.Value, q1),q2);

            // Move entity
            translation.Value.x += rock.dx * Time.DeltaTime * 60.0f;
            translation.Value.y += rock.dy * Time.DeltaTime * 60.0f;
            translation.Value.z += rock.dz * Time.DeltaTime * 60.0f;
            if (translation.Value.x > GameInformation.MaximumScreenEdge.x)
                translation.Value.x -= (GameInformation.MaximumScreenEdge.x - GameInformation.MinimumScreenEdge.x);
            if (translation.Value.x < GameInformation.MinimumScreenEdge.x)
                translation.Value.x += (GameInformation.MaximumScreenEdge.x - GameInformation.MinimumScreenEdge.x);
            if (translation.Value.z > GameInformation.MaximumScreenEdge.z)
                translation.Value.z -= (GameInformation.MaximumScreenEdge.z - GameInformation.MinimumScreenEdge.z);
            if (translation.Value.z < GameInformation.MinimumScreenEdge.z)
                translation.Value.z += (GameInformation.MaximumScreenEdge.z - GameInformation.MinimumScreenEdge.z);
        });
    }
}

