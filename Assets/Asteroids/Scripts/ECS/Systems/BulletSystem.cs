using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class BulletSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if ((GamePlay.Myself.state != GamePlay.GameState.gamePlaying) && (GamePlay.Myself.state != GamePlay.GameState.shieldShip)) return;

        Entities.WithAll<Bullet>().ForEach((ref Translation translation, ref Rotation rotation, ref Bullet bullet) =>
        {
            float3 tmpTranslation = translation.Value;
            Bullet tmpBullet = bullet;

            // Check with all rocks
            Entities.WithAll<Rock>().ForEach((ref Translation rockTranslation, ref Rotation rockRotation, ref Rock rock) =>
            {
                float distance = math.distance(tmpTranslation, rockTranslation.Value);
                if (distance < (rock.collisionRadius + tmpBullet.collisionRadius))
                {
                    // bullet hits rock
                    GamePlay.Myself.SplitRock(rock, rockTranslation.Value);
                    EntityManager.DestroyEntity(rock.entity);

                    EntityManager.DestroyEntity(tmpBullet.entity);
                    return;
                }
            });

            // Check with all Saucers
            Entities.WithAll<Saucer>().ForEach((ref Translation saucerTranslation, ref Rotation saucerRotation, ref Saucer saucer) =>
            {
                float distance = math.distance(tmpTranslation, saucerTranslation.Value);
                if (distance < (saucer.collisionRadius + tmpBullet.collisionRadius))
                {
                    GamePlay.Myself.KillSaucer(saucerTranslation.Value);
                    EntityManager.DestroyEntity(saucer.entity);
                    EntityManager.DestroyEntity(tmpBullet.entity);
                    return;
                }
            });

            translation.Value += bullet.speed * Time.DeltaTime * math.forward(rotation.Value);

            if ((translation.Value.x > GameInformation.MaximumScreenEdge.x) || (translation.Value.x < GameInformation.MinimumScreenEdge.x) ||
                (translation.Value.z > GameInformation.MaximumScreenEdge.z) || (translation.Value.z < GameInformation.MinimumScreenEdge.z))
            {
                EntityManager.DestroyEntity(bullet.entity);
            }
        });
    }
}