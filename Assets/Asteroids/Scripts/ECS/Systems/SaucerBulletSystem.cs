using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class SaucerBulletSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if ((GamePlay.Myself.state != GamePlay.GameState.gamePlaying) && (GamePlay.Myself.state != GamePlay.GameState.shieldShip)) return;

        Entities.WithAll<SaucerBullet>().ForEach((ref Translation translation, ref Rotation rotation, ref SaucerBullet saucerBullet) =>
        {
            if (GamePlay.Myself.state != GamePlay.GameState.shieldShip)
            {
                // collide with player
                if (Player.Myself.CollisionEnemy(translation.Value, saucerBullet.collisionRadius))
                {
                    EntityManager.DestroyEntity(saucerBullet.entity);
                    return;
                }
            }

            translation.Value += saucerBullet.speed * Time.DeltaTime * math.forward(rotation.Value);

            if ((translation.Value.x > GameInformation.MaximumScreenEdge.x) || (translation.Value.x < GameInformation.MinimumScreenEdge.x) ||
                (translation.Value.z > GameInformation.MaximumScreenEdge.z) || (translation.Value.z < GameInformation.MinimumScreenEdge.z))
            {
                EntityManager.DestroyEntity(saucerBullet.entity);
            }
        });
    }
}