using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class BonusSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if ((GamePlay.Myself.state != GamePlay.GameState.gamePlaying) && (GamePlay.Myself.state != GamePlay.GameState.shieldShip)) return;

        Entities.WithAll<Bonus>().ForEach((ref Translation translation, ref Rotation rotation, ref Bonus bonus) =>
        {
            // collide with player
            if (Player.Myself.CollisionBonus(translation.Value, bonus.collisionRadius))
            {
                EntityManager.DestroyEntity(bonus.entity);     // destroy the bonus when player touched
                return;
            }
            // Move entity
            translation.Value.x += bonus.dx * Time.DeltaTime * 60.0f; ;
            translation.Value.y += bonus.dy * Time.DeltaTime * 60.0f; ;
            translation.Value.z += bonus.dz * Time.DeltaTime * 60.0f; ;
            if (translation.Value.x > GameInformation.MaximumScreenEdge.x)
                translation.Value.x -= (GameInformation.MaximumScreenEdge.x - GameInformation.MinimumScreenEdge.x);
            if (translation.Value.x < GameInformation.MinimumScreenEdge.x)
                translation.Value.x += (GameInformation.MaximumScreenEdge.x - GameInformation.MinimumScreenEdge.x);
            if (translation.Value.z > GameInformation.MaximumScreenEdge.z)
                translation.Value.z -= (GameInformation.MaximumScreenEdge.z - GameInformation.MinimumScreenEdge.z);
            if (translation.Value.z < GameInformation.MinimumScreenEdge.z)
                translation.Value.z += (GameInformation.MaximumScreenEdge.z - GameInformation.MinimumScreenEdge.z);
                              
        });
    }
}

