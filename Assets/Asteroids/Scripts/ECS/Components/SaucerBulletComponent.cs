using Unity.Entities;

[GenerateAuthoringComponent]

public struct SaucerBullet : IComponentData
{
    public float speed;
    public float collisionRadius;
    public Entity entity;
}