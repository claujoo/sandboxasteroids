using Unity.Entities;

[GenerateAuthoringComponent]

public struct Saucer : IComponentData
{
    public float dx;
    public float dy;
    public float dz;
    public float collisionRadius;
    public float shootTimer;
    public Entity entity;
}