using Unity.Entities;

[GenerateAuthoringComponent]

public struct Star : IComponentData
{
    public float speed;
}