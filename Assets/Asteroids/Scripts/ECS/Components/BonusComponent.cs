using Unity.Entities;

[GenerateAuthoringComponent]

public struct Bonus : IComponentData
{
    public float dx;
    public float dy;
    public float dz;
    public float collisionRadius;
    public int type;
    public Entity entity;
}