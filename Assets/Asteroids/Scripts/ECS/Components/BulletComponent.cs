using Unity.Entities;

[GenerateAuthoringComponent]

public struct Bullet : IComponentData
{
    public float speed;
    public float collisionRadius;
    public Entity entity;
}