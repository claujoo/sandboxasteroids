using Unity.Entities;

[GenerateAuthoringComponent]

public struct Rock : IComponentData
{
    public float dx;
    public float dy;
    public float dz;
    public float collisionRadius;
    public int size;
    public float drotatex;
    public float drotatey;
    public Entity entity;
}