using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager Myself = null;

    public AudioClip musicMainMenu;
    public AudioClip musicGame;
    public AudioClip musicGameStart;
    public AudioClip musicGameOver;
    public AudioClip musicNextLevel;

    AudioSource[] audioTracks;

    void Awake()
    {
        if (Myself != null)
        {
            Debug.Log("ERROR there are more than 1 MusicManager.cs scripts attached in this scene.");
            Destroy(this.gameObject);
            return;
        }
        Myself = this;
        audioTracks = gameObject.GetComponents<AudioSource>();
        audioTracks[0].loop = true;
        audioTracks[0].clip = musicMainMenu;
        audioTracks[1].loop = false;
        audioTracks[1].Stop();
        audioTracks[0].Play();
    }

    IEnumerator GameStartSequence()
    {
        audioTracks[0].Stop();
        audioTracks[1].Stop();
        audioTracks[0].clip = musicGame;
        audioTracks[0].volume = 0.5f;
        audioTracks[1].clip = musicGameStart;
        audioTracks[1].Play();
        yield return new WaitForSeconds(1.0f);
        audioTracks[0].Play();
    }

    public void GameStart()
    {
        StartCoroutine(GameStartSequence());
    }


    public void NextLevel()
    {
        audioTracks[1].Stop();
        audioTracks[1].clip = musicNextLevel;
        audioTracks[1].Play();
    }

    IEnumerator GameOverSequence()
    {
        audioTracks[0].Stop();
        audioTracks[1].Stop();
        audioTracks[0].clip = musicMainMenu;
        audioTracks[0].volume = 1.0f;
        audioTracks[1].clip = musicGameOver;
        audioTracks[1].Play();
        yield return new WaitForSeconds(2.0f);
        audioTracks[0].Play();
    }

    public void GameOver()
    {
        StartCoroutine(GameOverSequence());
    }
}
