using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Collections;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

public class Player : MonoBehaviour
{
    public static Player Myself = null;

    private EntityManager entityManager;

    [SerializeField] private GameObject bulletPrefab = null;
    private Entity bulletEntityPrefab;

    float autoFireWaiting = 0.0f;

    public SphereCollider[] detectors;
    public GameObject ship;
    public GameObject redShip;
    public GameObject flame;

    Vector3 shipSpeed;

    float powerUp = 0.0f;

    private void Update()
    {
        if ((GamePlay.Myself.state == GamePlay.GameState.gamePlaying) || (GamePlay.Myself.state == GamePlay.GameState.shieldShip))
        {
            Vector3 shipPosition = shipSpeed + transform.position;
            shipSpeed = shipSpeed * GamePlay.ShipSlowdown;

            if (shipPosition.x > GameInformation.MaximumScreenShipEdge.x)
                shipPosition.x -= (GameInformation.MaximumScreenShipEdge.x - GameInformation.MinimumScreenShipEdge.x);
            if (shipPosition.x < GameInformation.MinimumScreenShipEdge.x)
                shipPosition.x += (GameInformation.MaximumScreenShipEdge.x - GameInformation.MinimumScreenShipEdge.x);
            if (shipPosition.z > GameInformation.MaximumScreenShipEdge.z)
                shipPosition.z -= (GameInformation.MaximumScreenShipEdge.z - GameInformation.MinimumScreenShipEdge.z);
            if (shipPosition.z < GameInformation.MinimumScreenShipEdge.z)
                shipPosition.z += (GameInformation.MaximumScreenShipEdge.z - GameInformation.MinimumScreenShipEdge.z);

            transform.position = shipPosition;


            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (!flame.activeInHierarchy)
                {
                    SoundManager.Myself.Woosh();
                    flame.SetActive(true);
                }
                float angle = transform.localEulerAngles.y * Mathf.Deg2Rad;
                shipSpeed.x += Mathf.Sin(angle) * GamePlay.ShipBurst / 2.0f * Time.deltaTime;
                shipSpeed.z += Mathf.Cos(angle) * GamePlay.ShipBurst / 2.0f * Time.deltaTime;
            }
            else
            {
                if (flame.activeInHierarchy)
                    flame.SetActive(false);
            }

            if (Input.GetKey(KeyCode.RightArrow))
                transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y + GamePlay.ShipRotationSpeed * Time.deltaTime, 0);
            if (Input.GetKey(KeyCode.LeftArrow))
                transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y - GamePlay.ShipRotationSpeed * Time.deltaTime, 0);

            if (Input.GetKey(KeyCode.Space))
            {
                // Auto Fire
                autoFireWaiting -= Time.deltaTime;
                powerUp -= Time.deltaTime;
                if (powerUp < 0.0f) powerUp = 0.0f;
                if (autoFireWaiting < 0)
                {
                    SoundManager.Myself.Fire();

                    autoFireWaiting = GamePlay.AutoFireWaitingPeriod;
                    SpawnOneBullet(transform.position, transform.rotation);
                    if (powerUp != 0.0f)
                    {
                        SpawnOneBullet(transform.position, transform.rotation * Quaternion.Euler(Vector3.up * 10));
                        SpawnOneBullet(transform.position, transform.rotation * Quaternion.Euler(Vector3.up * (-10)));
                    }
                }
            }
            else
                autoFireWaiting = 0.0f;
        }
        else
        {
            if (flame.activeInHierarchy)
                flame.SetActive(false);
        }
    }

    void Awake()
    {
        if (Myself != null)
        {
            Debug.Log("ERROR there are more than 1 Player.cs scripts attached in this scene.");
            Destroy(this.gameObject);
            return;
        }
        Myself = this;
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    public void Clean()
    {
        entityManager.CleanEntitiesOfType<Bullet>();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        bulletEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletPrefab, settings);

        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
        shipSpeed = Vector3.zero;

        ship.SetActive(true);
        redShip.SetActive(false);
    }

    // SPAWN BULLET
    // SPAWN BULLET
    void SpawnOneBullet(Vector3 position,Quaternion rotation)
    {
        CapsuleCollider prefabCollider = bulletPrefab.GetComponent<CapsuleCollider>();

        Entity tmpEntity = entityManager.Instantiate(bulletEntityPrefab);
        entityManager.SetComponentData(tmpEntity, new Translation { Value = new float3(position.x, position.y, position.z) + 3.0f * math.forward(rotation) });
        entityManager.SetComponentData(tmpEntity, new Rotation { Value = rotation });
        entityManager.SetComponentData(tmpEntity, new Bullet {  speed = GamePlay.BulletSpeed,
                                                                collisionRadius = prefabCollider.radius,
                                                                entity = tmpEntity });
    }

    // COLLISIONS PLAYER - ENEMY (can be Saucer / Rock / ... future enemies) REACTION
    // COLLISIONS PLAYER - ENEMY
    public bool CollisionEnemy(float3 translation,float radius)
    {
        Vector3 vectorTranslation = new Vector3(translation.x, translation.y, translation.z);

        foreach(SphereCollider col in detectors)
        {
            if (Vector3.Distance(vectorTranslation,col.transform.position) < (radius + col.radius))
            {
                SoundManager.Myself.PlayerDeath();

                StartCoroutine(ExplodeManager("PlayerExplosion",transform.position));
                ship.SetActive(false);
                redShip.SetActive(true);

                if (GamePlay.Myself.lives > 1)
                {
                    GamePlay.Myself.lives--;
                    StartCoroutine(ShieldMakerAfterBeingHit());
                }
                else
                {
                    SoundManager.Myself.StopBipLoop();
                    GamePlay.Myself.state = GamePlay.GameState.gameOver;
                }
                return (true);
            }
        }
        return (false);
    }

    // COLLISION PLAYER - BONUS REACTION
    // COLLISION PLAYER - BONUS
    public bool CollisionBonus(float3 translation, float radius)
    {
        Vector3 vectorTranslation = new Vector3(translation.x, translation.y, translation.z);

        foreach (SphereCollider col in detectors)
        {
            if (Vector3.Distance(vectorTranslation, col.transform.position) < (radius + col.radius))
            {
                SoundManager.Myself.PickupBonus();
                powerUp = 5.0f;
                return (true);
            }
        }
        return (false);
    }

    // TIMED ACTIONS
    // TIMED ACTIONS
    IEnumerator ExplodeManager(string effectPrefabName, Vector3 tmpPosition)
    {
        GameObject obj = Instantiate(Resources.Load(effectPrefabName)) as GameObject;
        obj.transform.position = tmpPosition;
        yield return new WaitForSeconds(2.0f);
        Destroy(obj);
    }

    IEnumerator ShieldMakerAfterBeingHit()
    {
        GamePlay.Myself.state = GamePlay.GameState.deadShip;
        yield return new WaitForSeconds(2.0f);
        redShip.SetActive(false);
        shipSpeed = Vector3.zero;       // halt the Ship
        GamePlay.Myself.state = GamePlay.GameState.shieldShip;
        for (int i = 0; i < 10; i++)      // blink 10 times
        {
            ship.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            ship.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
        ship.SetActive(true);
        GamePlay.Myself.state = GamePlay.GameState.gamePlaying;
    }

}
