using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject pressSpaceText;
    public GameObject menuCanvas;
    public RectTransform logoRectTransform;

    float wobbleValue = 0.0f;
    float logoStartY = 0.0f;

    private void Awake()
    {
        logoStartY = logoRectTransform.position.y;
    }

    private void OnEnable()
    {
        StartCoroutine(BlinkingPressSpaceText());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void Update()
    {
        wobbleValue += 0.05f * (Time.deltaTime * 50.0f);
        logoRectTransform.position = new Vector3(logoRectTransform.position.x, logoStartY + Mathf.Sin(wobbleValue) * 12.0f, logoRectTransform.position.z);

        if (Input.GetKeyUp(KeyCode.Space))
        {
            SoundManager.Myself.Button();
            MusicManager.Myself.GameStart();
            GamePlay.Myself.ResetGame();
            menuCanvas.SetActive(false);
            gameObject.SetActive(false);
        }
    }

    // TIMED ACTIONS
    // TIMED ACTIONS
    IEnumerator LogoUpAndDown()
    {
        while (true)
        {
            pressSpaceText.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            pressSpaceText.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator BlinkingPressSpaceText()
    {
        while (true)
        {
            pressSpaceText.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            pressSpaceText.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
    }
}

