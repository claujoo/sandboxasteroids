using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Collections;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

public class SpawnSaucerAndBonus : MonoBehaviour
{
    public static SpawnSaucerAndBonus Myself = null;

    [SerializeField] private GameObject saucerPrefab = null;
    Entity saucerEntityPrefab;
    [SerializeField] private GameObject saucerBulletPrefab = null;
    Entity saucerBulletEntityPrefab;
    [SerializeField] private GameObject bonusPrefab = null;
    Entity bonusEntityPrefab;

    EntityManager entityManager;

    void Awake()
    {
        if (Myself != null)
        {
            Debug.Log("ERROR there are more than 1 SpawnSaucerAndBonus.cs scripts attached in this scene.");
            Destroy(this.gameObject);
            return;
        }
        Myself = this;
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    public void Clean()
    {
        StopAllCoroutines();
        bonusCounter = 0;
        bonusCumulator = GamePlay.BonusAfterXRocksHit;
        entityManager.CleanEntitiesOfType<Saucer>();
        entityManager.CleanEntitiesOfType<SaucerBullet>();
        entityManager.CleanEntitiesOfType<Bonus>();

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        saucerEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(saucerPrefab, settings);
        saucerBulletEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(saucerBulletPrefab, settings);
        bonusEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(bonusPrefab, settings);
        StartCoroutine(SpawnSaucer());
    }

    // SPAWN BONUS CALL : 'TRY' because the spawning only is valid under certain condition(s)
    // SPAWN BONUS CALL
    int bonusCounter = 0;
    int bonusCumulator = 0;

    public void TrySpawnBonus(Vector3 position,Vector3 deltaDirection)
    {
        bonusCounter++;
        if (bonusCounter == bonusCumulator)
        {
            bonusCumulator += GamePlay.BonusAfterXRocksHit;
            SphereCollider prefabCollider = bonusPrefab.GetComponent<SphereCollider>();

            Entity tmpEntity = entityManager.Instantiate(bonusEntityPrefab);
            entityManager.SetComponentData(tmpEntity, new Translation { Value = position });
            entityManager.SetComponentData(tmpEntity, new Rotation { Value = Quaternion.identity });
            entityManager.SetComponentData(tmpEntity, new Bonus
            {
                dx = deltaDirection.x,
                dy = deltaDirection.y,
                dz = deltaDirection.z,
                collisionRadius = prefabCollider.radius,
                entity = tmpEntity
            });

            bonusCounter = 0;
        }
    }

    // SPAWN SAUCER BULLET
    // SPAWN SAUCER BULLET
    public void SpawnSaucerBullet(float3 position)
    {
        SoundManager.Myself.SaucerFire();

        CapsuleCollider prefabCollider = saucerBulletPrefab.GetComponent<CapsuleCollider>();

        Vector3 relativePos = Player.Myself.transform.position - new Vector3(position.x, position.y, position.z);
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);

        Entity tmpEntity = entityManager.Instantiate(saucerBulletEntityPrefab);
        entityManager.SetComponentData(tmpEntity, new Translation { Value = position });
        entityManager.SetComponentData(tmpEntity, new Rotation { Value = rotation });
        entityManager.SetComponentData(tmpEntity, new SaucerBullet
        {
            speed = GamePlay.SaucerBulletSpeed,
            collisionRadius = prefabCollider.radius,
            entity = tmpEntity
        });
    }

    // TIMED ACTIONS
    // TIMED ACTIONS

    // This endless loop spawns Saucer entities depending a certain interval
    IEnumerator SpawnSaucer()
    {
        while (true)
        {
            yield return new WaitForSeconds(GamePlay.SaucerAppearInterval);
            if ((GamePlay.Myself.state == GamePlay.GameState.gamePlaying) || (GamePlay.Myself.state == GamePlay.GameState.shieldShip))
            {
                SphereCollider prefabCollider = saucerPrefab.GetComponent<SphereCollider>();

                Vector3 position = new Vector3(GameInformation.MinimumScreenEdge.x + 0.5f, 0, UnityEngine.Random.Range(GameInformation.MinimumScreenEdge.z / 2.0f, GameInformation.MaximumScreenEdge.z / 2.0f));
                float speedAndDirection = GamePlay.SaucerSpeed;
                if (UnityEngine.Random.Range(0, 10) > 5)
                    speedAndDirection = -GamePlay.SaucerSpeed;
                if (entityManager.CountEntitiesOfType<Saucer>() == 1)
                    SoundManager.Myself.BipLoop();
                Entity tmpEntity = entityManager.Instantiate(saucerEntityPrefab);
                entityManager.SetComponentData(tmpEntity, new Translation { Value = position });
                entityManager.SetComponentData(tmpEntity, new Rotation { Value = Quaternion.identity });
                entityManager.SetComponentData(tmpEntity, new Saucer
                {
                    dx = speedAndDirection,
                    dy = 0,
                    dz = 0,
                    collisionRadius = prefabCollider.radius,
                    shootTimer = GamePlay.SaucerShootInterval,
                    entity = tmpEntity
                });
            }
        }
    }

}
