using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Unity.Entities;
using Unity.Collections;

public static class Helper
{
    public static void CleanEntitiesOfType<T>(this EntityManager entityManager)
    {
        NativeArray<Entity> allEntities = entityManager.GetAllEntities();

        foreach (Entity entity in allEntities)
        {
            if (entityManager.HasComponent<T>(entity))
                entityManager.DestroyEntity(entity);
        }
    }

    public static int CountEntitiesOfType<T>(this EntityManager entityManager)
    {
        int counter = 0;
        NativeArray<Entity> allEntities = entityManager.GetAllEntities();

        foreach (Entity entity in allEntities)
        {
            if (entityManager.HasComponent<T>(entity))
                counter++;
        }
        return (counter);
    }
}