using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Myself = null;

    public AudioClip bipLoop;
    public AudioClip button;
    public AudioClip big_explode;
    public AudioClip medium_explode;
    public AudioClip small_explode;
    public AudioClip fire;
    public AudioClip hishot;
    public AudioClip pickupbonus;
    public AudioClip woosh;
    public AudioClip saucerexplode;
    public AudioClip playerDeath;

    AudioSource[] audioTracks;

    void Awake()
    {
        if (Myself != null)
        {
            Debug.Log("ERROR there are more than 1 SoundManager.cs scripts attached in this scene.");
            Destroy(this.gameObject);
            return;
        }
        Myself = this;
        audioTracks = gameObject.GetComponents<AudioSource>();
    }

    void PlaySound(AudioClip clip,int channel,bool loop = false)
    {
        audioTracks[channel].Stop();
        audioTracks[channel].clip = clip;
        audioTracks[channel].loop = loop;
        audioTracks[channel].Play();

    }

    public void Button()
    {
        PlaySound(button, 1);
    }

    public void Fire()
    {
        PlaySound(fire, 1);
    }

    public void SaucerFire()
    {
        PlaySound(hishot, 2);
    }

    public void Woosh()
    {
        PlaySound(woosh, 4);
    }

    public void PickupBonus()
    {
        PlaySound(pickupbonus, 2);
    }

    public void SaucerExplode()
    {
        PlaySound(saucerexplode, 3);
    }

    public void Big_explode()
    {
        PlaySound(big_explode, 2);
    }

    public void Medium_explode()
    {
        PlaySound(medium_explode, 3);
    }

    public void Small_explode()
    {
        PlaySound(small_explode, 4);
    }

    public void PlayerDeath()
    {
        PlaySound(playerDeath, 2);
    }

    public void BipLoop()
    {
        PlaySound(bipLoop, 0, true);
    }

    public void StopBipLoop()
    {
        audioTracks[0].Stop();
    }
}
