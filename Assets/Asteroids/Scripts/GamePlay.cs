using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Collections;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

public class GamePlay : MonoBehaviour
{
    // GAME PARAMETERS
    public static float BulletSpeed = 60.0f;
    public static float AutoFireWaitingPeriod = 0.2f;
    public static float ShipBurst = 1.2f;
    public static float ShipSlowdown = 0.96f;
    public static float ShipRotationSpeed = 120.0f;
    public static int ScoreSmallRock = 100;
    public static int ScoreMediumRock = 50;
    public static int ScoreBigRock = 20;
    public static int ScoreSaucer = 300;
    public static int BonusAfterXRocksHit = 30;
    public static float SaucerAppearInterval = 10.0f;
    public static float SaucerShootInterval = 3.0f;
    public static float SaucerSpeed = 0.2f;
    public static float SaucerBulletSpeed = 20.0f;
    // GAME PARAMETERS

    public enum GameState
    {
        mainMenu,
        gamePlaying,
        deadShip,
        shieldShip,
        gameOver
    }

    public static GamePlay Myself = null;

    private EntityManager entityManager;

    [SerializeField] private GameObject bigRockPrefab = null;
    private Entity bigRockEntityPrefab;

    [SerializeField] private GameObject mediumRockPrefab = null;
    private Entity mediumRockEntityPrefab;

    [SerializeField] private GameObject smallRockPrefab = null;
    private Entity smallRockEntityPrefab;

    public GameState state = GameState.mainMenu;

    public int lastLevel = 1;
    public int lastLives = 3;
    public int level = 1;
    public int score = 1;
    public int lives = 3;
    float rockSpeed = 1.0f;

    void Awake()
    {
        if (Myself != null)
        {
            Debug.Log("ERROR there are more than 1 GamePlay.cs scripts attached in this scene.");
            Destroy(this.gameObject);
            return;
        }

        Myself = this;
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    void Clean()
    {
        entityManager.CleanEntitiesOfType<Rock>();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        bigRockEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(bigRockPrefab, settings);
        mediumRockEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(mediumRockPrefab, settings);
        smallRockEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(smallRockPrefab, settings);
    }

    // INITIALIZE AND RESET GAME
    // INITIALIZE AND RESET GAME
    public void ResetGame()
    {
        lastLives = 2;
        lives = 3;
        level = 0;
        lastLevel = 0;
        score = 0;
        state = GamePlay.GameState.gamePlaying;
        SpawnSaucerAndBonus.Myself.Clean();
        LevelUp();
    }

    public void LevelUp()
    {
        Clean();
        Player.Myself.Clean();
        level++;
        InitializeLevel();
    }

    void InitializeLevel()
    {
        for (int i = 0;i < (level + 2);i++)
        {
            float centerDistance = 20.0f + UnityEngine.Random.Range(0, 20.0f);

            float rotation = UnityEngine.Random.Range(0, Mathf.PI * 2.0f);
            Vector3 position = new Vector3(Mathf.Sin(rotation) * centerDistance, 0.0f, Mathf.Cos(rotation) * centerDistance);

            int quadrant = (int)(UnityEngine.Random.Range(0, 4));
            float direction = UnityEngine.Random.Range(0, Mathf.PI / 3.0f) + (Mathf.PI / 12.0f) + (Mathf.PI / 2.0f) * quadrant;
            Vector3 deltaDirection = new Vector3(Mathf.Sin(direction) * rockSpeed / 10.0f, 0.0f, Mathf.Cos(direction) * rockSpeed / 10.0f);
            SpawnBigRock(position, Quaternion.identity, deltaDirection);
        }
    }

    // SPAWN ROCKS OF 3 TYPES
    // SPAWN ROCKS OF 3 TYPES
    void SpawnRock(Entity newEntity,Vector3 position, Quaternion rotation, Vector3 deltaDirection,float radius,int parameterSize)
    {
        entityManager.SetComponentData(newEntity, new Translation { Value = new float3(position.x, position.y, position.z) });
        entityManager.SetComponentData(newEntity, new Rotation { Value = rotation });
        entityManager.SetComponentData(newEntity, new Rock
        {
            dx = deltaDirection.x,
            dy = deltaDirection.y,
            dz = deltaDirection.z,
            drotatex = UnityEngine.Random.Range(-0.5f,0.5f),
            drotatey = UnityEngine.Random.Range(-0.5f, 0.5f),
            collisionRadius = radius,
            size = parameterSize,
            entity = newEntity
        });
    }

    void SpawnBigRock(Vector3 position, Quaternion rotation,Vector3 deltaDirection)
    {
        SphereCollider prefabCollider = bigRockPrefab.GetComponent<SphereCollider>();
        SpawnRock(entityManager.Instantiate(bigRockEntityPrefab),position,rotation,deltaDirection, prefabCollider.radius, 3);
    }

    void SpawnMediumRock(Vector3 position, Quaternion rotation, Vector3 deltaDirection)
    {
        SphereCollider prefabCollider = mediumRockPrefab.GetComponent<SphereCollider>();
        SpawnRock(entityManager.Instantiate(mediumRockEntityPrefab), position, rotation, deltaDirection, prefabCollider.radius, 2);
    }

    void SpawnSmallRock(Vector3 position, Quaternion rotation, Vector3 deltaDirection)
    {
        SphereCollider prefabCollider = smallRockPrefab.GetComponent<SphereCollider>();
        SpawnRock(entityManager.Instantiate(smallRockEntityPrefab), position, rotation, deltaDirection, prefabCollider.radius, 1);
    }


    // ROCK SPLITTING
    // ROCK SPLITTING
    public void SplitRock(Rock rock,float3 position)
    {
        int addScore = ScoreSmallRock;
        Vector3 tmpPosition = new Vector3(position.x, position.y, position.z);
        if (rock.size > 1)          // not the smallest rock
        {
            Vector3 deltaDirection1 = new Vector3(rock.dz,0,rock.dx);
            Vector3 deltaDirection2 = new Vector3(-rock.dz, 0, -rock.dx);
            switch (rock.size)
            {
                case 3 :
                    SoundManager.Myself.Big_explode();
                    SpawnMediumRock(tmpPosition, Quaternion.identity, deltaDirection1);
                    SpawnMediumRock(tmpPosition, Quaternion.identity, deltaDirection2);
                    StartCoroutine(ExplodeManager("BigExplosion", tmpPosition));
                    addScore = ScoreBigRock;
                    break;
                case 2:
                    SoundManager.Myself.Medium_explode();
                    SpawnSmallRock(tmpPosition, Quaternion.identity, deltaDirection1);
                    SpawnSmallRock(tmpPosition, Quaternion.identity, deltaDirection2);
                    StartCoroutine(ExplodeManager("MediumExplosion", tmpPosition));
                    addScore = ScoreMediumRock;
                    break;
            }
        }
        else
        {
            SoundManager.Myself.Small_explode();
            StartCoroutine(ExplodeManager("SmallExplosion", tmpPosition));
            StartCoroutine(DelayCheckEndOfLevel());
        }

        SpawnSaucerAndBonus.Myself.TrySpawnBonus(tmpPosition, new Vector3(-rock.dx, 0, -rock.dz));
        AddValueToScore(addScore);
        StartCoroutine(FlyingScore(tmpPosition, addScore));
    }

    // SAUCER DEATH
    // SAUCER DEATH
    public void KillSaucer(float3 position)
    {
        StartCoroutine(KillSaucerStopBip());
        SoundManager.Myself.SaucerExplode();
        Vector3 tmpPosition = new Vector3(position.x, position.y, position.z);
        StartCoroutine(ExplodeManager("MediumExplosion", tmpPosition));
        AddValueToScore(ScoreSaucer);
        StartCoroutine(FlyingScore(tmpPosition, ScoreSaucer));
    }

    void AddValueToScore(int addScore)
    {
        if ((score / 10000) != ((score + addScore) / 10000))   // extra live
        {
            if (lives < 7)
                lives++;
        }
        score += addScore;
    }


    // TIMED ACTIONS
    // TIMED ACTIONS
    IEnumerator DelayCheckEndOfLevel()
    {
        yield return new WaitForSeconds(0.3f);
        if (entityManager.CountEntitiesOfType<Rock>() <= 3)  // 3 rock types as prefabs in entities
            LevelUp();
    }

    IEnumerator ExplodeManager(string effectPrefabName,Vector3 tmpPosition)
    {
        GameObject obj = Instantiate(Resources.Load(effectPrefabName)) as GameObject;
        obj.transform.position = tmpPosition;
        yield return new WaitForSeconds(2.0f);
        Destroy(obj);
    }

    IEnumerator FlyingScore(Vector3 position, int value)
    {
        float wobble = 0.0f;
        GameObject obj = Instantiate(Resources.Load("ScoreText")) as GameObject;
        obj.transform.position = position;
        TextMesh tm = obj.GetComponentInChildren<TextMesh>();
        tm.text = value.ToString();
        float alpha = 1.0f;
        while (alpha > 0.0f)
        {
            wobble += 0.5f;
            Vector3 v = obj.transform.localEulerAngles;
            v.y = Mathf.Sin(wobble) * 12.0f;
            obj.transform.localEulerAngles = v;
            alpha -= Time.deltaTime * 5.0f;
            if (alpha < 0.0f)
                alpha = 0.0f;
            tm.color = new Color(tm.color.r, tm.color.g, tm.color.b, alpha);
            yield return new WaitForSeconds(0.03f);
        }
        Destroy(obj);
    }

    IEnumerator KillSaucerStopBip()
    {
        yield return new WaitForSeconds(0.3f);
        if (entityManager.CountEntitiesOfType<Saucer>() == 1)
            SoundManager.Myself.StopBipLoop();
    }
}
