using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Collections;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;

public class StarField : MonoBehaviour
{
    [SerializeField] private GameObject starPrefab = null;

    void Awake()
    {
        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        Entity starEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(starPrefab, settings);

        for(int i=0;i<200;i++)
        {
            Vector3 position = Vector3.zero;
            position.x = UnityEngine.Random.Range(-80.0f,80.0f);
            position.y = UnityEngine.Random.Range(-100.0f, 50.0f);
            position.z = UnityEngine.Random.Range(-80.0f, 80.0f);
            Quaternion rotation = Quaternion.identity;

            Entity tmpEntity = entityManager.Instantiate(starEntityPrefab);
            entityManager.SetComponentData(tmpEntity, new Translation { Value = position });
            entityManager.SetComponentData(tmpEntity, new Rotation { Value = rotation });
            entityManager.SetComponentData(tmpEntity, new Star
            {
                speed = 10.0f
            });
        }
        entityManager.DestroyEntity(starEntityPrefab);
        Destroy(gameObject);    // No need anymore
    }

}
