﻿Shader "Custom/Outline"
{
	Properties//Variables
	{
		_OutlineColor("Outline Color", Color) = (1,1,1,1)
		_OutlineWidth("Outline Width", Range(1.0,10.0)) = 1.1
	}

	SubShader
	{
		Tags 
		{
			"Queue" = "Transparent"
		}
		Pass
		{
			Name "OBJECT"
			ZWrite Off//Allows for other render passes to be drawn on top of this pass.
			Colormask 0
			
			Stencil
			{
				Ref 1
				Comp Always
				Pass Replace
			}
							CGPROGRAM//Allows talk between two languages: shader lab and nvidia C for graphics.

						//\===========================================================================================
						//\ Function Defines - defines the name for the vertex and fragment functions
						//\===========================================================================================

						#pragma vertex vert//Define for the building function.

						#pragma fragment frag//Define for coloring function.

						//\===========================================================================================
						//\ Includes
						//\===========================================================================================

						#include "UnityCG.cginc"//Built in shader functions.

						//\===========================================================================================
						//\ Structures - Can get data like - vertices's, normal, color, uv.
						//\===========================================================================================

						struct appdata//How the vertex function receives info.
						{
							float4 vertex : POSITION;
							float2 uv : TEXCOORD0;
						};

						struct v2f
						{
							float4 pos : SV_POSITION;
							float2 uv : TEXCOORD0;
						};

						//\===========================================================================================
						//\ Vertex Function - Builds the object
						//\===========================================================================================

						v2f vert(appdata IN)
						{
							v2f OUT;
							float4 mypos = UnityObjectToClipPos(IN.vertex);
							mypos.z += 1.0;
							OUT.pos = mypos;
							OUT.uv = IN.uv;

							return OUT;
						}

						//\===========================================================================================
						//\ Fragment Function - Color it in
						//\===========================================================================================

						fixed4 frag(v2f IN) : SV_Target
						{
							return fixed4(0.5,0,0,1);
						}

						ENDCG
					}
					Pass
		{
			Name "OUTLINE"
//			ZWrite Off//Allows for other render passes to be drawn on top of this pass.
	
					Stencil
					 {
						 Ref 1
						 Comp NotEqual
						Pass Replace
					 }
			CGPROGRAM//Allows talk between two languages: shader lab and nvidia C for graphics.

			//\===========================================================================================
			//\ Function Defines - defines the name for the vertex and fragment functions
			//\===========================================================================================

			#pragma vertex vert//Define for the building function.

			#pragma fragment frag//Define for coloring function.

			//\===========================================================================================
			//\ Includes
			//\===========================================================================================

			#include "UnityCG.cginc"//Built in shader functions.

			//\===========================================================================================
			//\ Structures - Can get data like - vertices's, normal, color, uv.
			//\===========================================================================================

			struct appdata//How the vertex function receives info.
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			//\===========================================================================================
			//\ Imports - Re-import property from shader lab to nvidia cg
			//\===========================================================================================

			float _OutlineWidth;
			float4 _OutlineColor;

			//\===========================================================================================
			//\ Vertex Function - Builds the object
			//\===========================================================================================

			v2f vert(appdata IN)
			{
				IN.vertex.xyz *= _OutlineWidth;
				v2f OUT;

				OUT.pos = UnityObjectToClipPos(IN.vertex);
				OUT.uv = IN.uv;

				return OUT;
			}

			//\===========================================================================================
			//\ Fragment Function - Color it in
			//\===========================================================================================

			fixed4 frag(v2f IN) : SV_Target
			{
				return _OutlineColor;
			}

			ENDCG
		}

	}
}
