## **ASTEROIDS**
By Claude Verstraeten

The Sandbox team requested a game to be made as a recruitement test... this is it.

## Controls

Arrow Key Up : Burst engine
Arrow Key Left : Turn ship left
Arrow Key Right : Turn ship right
Space bar : Start Game / Fire bullet(s)

## Installs needed

You will need to have the following installed or ready to use.

1. Unity 2020.2.3f1, download it from : https://unity3d.com/get-unity/download/archive


## Project progress

All the steps taken to make this project.

1. Create BitBucket Git project, link Mac SourceTree to the Git.
2. Create Unity project and make sure we are in the right Git directory.
3. Open the package manager and clean the packages not needed.
4. Add packages : com.unity.entities ; com.unity.dots.editor; com.unity.mathematics; com.unity.rendering.hybrid
5. Create first Entity testing : Assets/Dots_Testing folder contains the test source and assets
6. Spawning and moving of entities understood
7. Learned the hard way that Entities do not enter into the regular collision scheme
8. Lets make some simple collision detection
9. Archive version before sleep day 1 is done
10. Made a player object and turn it around
11. created a bullet pool and made edges for the screen so bullets will be destroyed when out of screen
12. created a rock pool and used the same edges for wrapping around the scene
13. Collisions between bullets and rocks, make the split of rocks
14. Collisions Space ship and rocks
15. Imported font and setup the HUD, integrated score, level number, lives and some messages
16. Made the game logic. Start / Clear level / Die / Game over / Level up / ... with all the needed links to make it work
17. Struggled a little bit with the entity clean up. Solution isn't brilliant, I am sure it can be more elegant
18. Spotted a typo in the splitting... fixed
19. Go a bunch of 3D rock objects, integrated them and struggled a little bit over Unity.Mathematics library as the quaternions are inside the Rotation of the entity. Made the rocks spin around at some random speeds.
20. Made a player ship in 3D and imported it. Added the flame for the next step ... velocity flying
21. Score text in the gameplay.. with wobble
22. I added a smaller world-wrap set of boundaries for the spaceship so you will not lose it off-screen
23. I tried with some rigid body and velocity but actually simple vectors for the ships burst are a lot easier and straight forward
24. Glow effect is back and applied on everything
25. Added music and changed the camera for the HUD order spotted a not so good effect on the spaceship-rocks collision
26. day 2 is done ... tomorrow morning I finalize the game
27. Added new Entity components : Bonus ; Saucer; SaucerBullet; Star
28. Made a starfield ... using the fog
29. Created a 3d Saucer and integrated it into the Saucer entity structure and spawn it
30. Create Bonus as an Entity, decide interval and decide bonus reaction -> apply on ship
31. Flying Saucer destroy Player, Bullet destroys Saucer + dancing score
32. Saucer Bullets
33. Sound time ... cleaning up sound effects and music to be used
34. Implement the sound!!

## Game information

Overal game tuning : GamePlay.cs at the start there are GAME PARMETERS to adjust the gameplay of the game

Script functionality:

## ECS

**BonusComponent.cs / BonusSystem.cs** :

Move a bonus object around with edge position wrapping
Upon collision with the Player the "PowerUp" timer will be set and the player will get a shooting power up.

**BulletComponent.cs / BulletSystem.cs** :

Move a bullet straight forward
Check collisions with Rocks and Saucers
No position wrapping instead the Bullet is destroyed on the edge

**RockComponent.cs / RockSystem.cs** :

Move a Rock using a delta propulsion
Edge position wrapping
Rocks spin randomly for visual animation effect
The delta position update also happens on the Y axis which is not really needed.

**SaucerComponent.cs / SaucerSystem.cs** :

A saucer moves currently only over the X axis, but x,y and z are used and being updated, just in case we would like to extend the behaviour of Saucers
Edge position wrapping
Every predefined time interval the Saucer fires into the Players direction

**SaucerBulletComponent.cs / SaucerBulletSystem.cs** :

Move a bullet straight forward
Check collisions with Player
No position wrapping instead the Bullet is destroyed on the edge


**StarComponent.cs / StarSystem.cs** :

Move stars on the Y axis and reset them to the back

## Game Logic Sources

**GamePlay.cs**

Centralizes the globale gameplay settings
Initialize / ResetGame / Level UP : this call the Clean-entity methods of GamePlay, SpawnSaucerAndBonus and Player
Contains the calls to spawn 3 Rock sizes
Saucer death reaction
Rock death reaction with rock splitting
Checks the End Of Level
Times-Display of local gained scores

**SpawnSaucerAndBonus.cs**

Uses a Timer to spawn Saucer objects and hold the call to spawn Bonus and Saucer Fire bullets
Bonus spawn is called from Rock exploding in GamePlay.cs
Saucer Fire bullets is called from the Saucer Syste

**Player.cs**

This hold all the logical calls that concerns the Player.
The Update loop contains the player controls, position-edge looping, Player movement and Fire management
Collision with and Enemy (Rock or Saucer) calculation and reaction is handled here (called from the right Systems)
Collision with Bonus is handled here (called from the Bonus System)
And Timed management of the Shield and Explode Effect showing

**GameHud.cs**

The Update of GameHud keeps the hud elements up to date. It checks Game variables to do this.

**MainMenu.cs**

Blinks the Press Space text, wobbles the Title and waits until the space button was pressed, removes the main menu and Initializes the game

**GameInformation.cs**

Hold the global edge definitions of the screen in 3d. Being used troughout the Entity Systems

**Helper.cs**

Adds some methods to the EntityManager

**MusicManager.cs**

A bunch of AudioClips that are played using public calls on the public static MusicManager 

**SoundManager.cs**

A bunch of AudioClips that are played using public calls on the public static SoundManager 

**Starfield.cs**

Creates the starfield in the Entity list and destroys itself.

